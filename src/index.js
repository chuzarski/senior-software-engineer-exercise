const fs = require('fs');

const chirpViews = JSON.parse(fs.readFileSync(`${__dirname}/chirpViews.json`));

/*
This function will take as input a date string in ISO format and return a date string in MM/DD/YYYY format
*/
const formatDateString = (dateStr) => {
  date = new Date(dateStr);

  // adjust time for local timezone distance (avoiding off-by-one on the day)
  date.setMinutes(date.getMinutes() + date.getTimezoneOffset());

  // return formatted string
  return date.toLocaleDateString('en-US', {timezone: 'UTC', month: '2-digit', day: '2-digit', year:'numeric'});
};

/*
  This function will take as input a chirpView object and returns the meta-data related to this such as
  ' <date> <views> <author> <optional emoji> 
*/
const formatChirpMeta = (chirpView) => {

  let EMOJI_VIEW_THRESHOLD = 100000;
  let EMOJI_CODEPOINT = 0x1f525; // codepoint for fire emoji

  formattedChirpMeta = ' ' + formatDateString(chirpView.date) + ' ' + chirpView.views + ' ' + chirpView.author;

  if(chirpView.views >= EMOJI_VIEW_THRESHOLD) {
    formattedChirpMeta += ' ' + String.fromCodePoint(EMOJI_CODEPOINT);
  }

  return formattedChirpMeta;
}

/*
truncateMessage wil truncate the message, so that the entire formatted string will fit within `totalLength` characters.
In addition, `ending` will specify what is appended to the end of the string
*/
const truncateMessage = (message, metaLength, totalLength, ending) => {

  // strlenDiff is the number of characters we need to truncate from the message to fit
  // within a `totalLength` string
  var strlenDiff = message.length + metaLength - totalLength;

  return message.substring(0, (message.length - strlenDiff - ending.length)).concat(ending);
}

const formatChirpView = (chirpView) => {

  let FORMAT_STRING_TOTAL_LENGTH = 140;
  var formattedChirpMeta;

  // format meta-data related to chirp
  formattedChirpMeta = formatChirpMeta(chirpView);

  // determine if the message string requires truncation
  // if so, return truncated version + chirpMeta
  if((chirpView.message.length + formattedChirpMeta.length) > FORMAT_STRING_TOTAL_LENGTH) {
    return truncateMessage(chirpView.message, formattedChirpMeta.length, FORMAT_STRING_TOTAL_LENGTH, '...') + formattedChirpMeta;
  }

  // otherwise, return string as is
  return chirpView.message + formattedChirpMeta;
};

chirpViews.map((chirpView) => console.log(formatChirpView(chirpView)));


module.exports = {
  formatChirpView,
  formatDateString,
  formatChirpMeta,
  truncateMessage
};
