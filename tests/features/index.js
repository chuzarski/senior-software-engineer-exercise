const { formatChirpView, formatDateString, formatChirpMeta, truncateMessage } = require('../../src/index');
const assert = require('assert');
const { truncate } = require('fs');

describe('#formatChirpView', () => {
  it('should properly format a normal chirp view', () => {
    assert.strictEqual(
      'Hello World! 06/28/2021 99999 Jack Daniels',
      formatChirpView({
        message: 'Hello World!',
        author: 'Jack Daniels',
        views: 99999,
        date: '2021-06-28T07:00:00.000Z',
      })
    );
  });


  // I am not entirely sure if this is a mistake in the original test, but the original test does not call formatChirpView,
  // instead it compares "This chirp probably won't get a lot of attention. 03/04/2021 251236 Jane Doe 🔥"
  // directly to a chirpView object
  // This test will pass if compared to formatChirpView(obj)
  it('should properly add fire emoji when views exceed 100,000', () => {
    assert.strictEqual(
      "This chirp probably won't get a lot of attention. 03/04/2021 251236 Jane Doe 🔥",
      formatChirpView({
        message: "This chirp probably won't get a lot of attention.",
        author: 'Jane Doe',
        views: 251236,
        date: '2021-03-04T04:00:00.000Z',
      })
    );
  });

  it('should properly format long chirp messages', () => {
    assert.strictEqual(
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consequat quis turpis non consectetur. S... 01/01/2021 1000001 Lorem Ipsum 🔥',
      formatChirpView({
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consequat quis turpis non consectetur. Sed accumsan dui rhoncus, cursus quis.',
        author: 'Lorem Ipsum',
        views: 1000001,
        date: '2021-01-01T00:00:00.000Z',
      })
    );

    assert.strictEqual(
      "This is a maximum length chirp because I like writing long chirps. Why? I'm not really sure. Maybe I'm just a r... 04/22/2021 100 John Smith",
      formatChirpView({
        message:
          "This is a maximum length chirp because I like writing long chirps. Why? I'm not really sure. Maybe I'm just a rebel.. or maybe it's a phase?",
        author: 'John Smith',
        views: 100,
        date: '2021-04-22T00:00:00.000Z',
      })
    );
  });


  // Additional logic testing
  it('Date formatter should return correct date', () => {
    assert.strictEqual(formatDateString('2021-01-01T00:00:00.000Z'), '01/01/2021'); // tests midnight
    assert.strictEqual(formatDateString('2021-01-01T23:59:59.000Z'), '01/01/2021'); // tests minute before midnight
    assert.strictEqual(formatDateString('2021-03-04T04:00:00.000Z'), '03/04/2021'); // tests mid-day
  });

  it('Formatting function for chirp meta-data should properly format into a string', () => {
    assert.strictEqual(formatChirpMeta(
      {
        author: 'Admiral Kirk',
        views: 99999,
        date: '2021-03-04T04:00:00.000Z'
      }), " 03/04/2021 99999 Admiral Kirk"
    );

    assert.strictEqual(formatChirpMeta(
      {
        author: 'Chris Smith',
        views: 10000000,
        date: '2021-03-04T04:00:00.000Z'
      }), " 03/04/2021 10000000 Chris Smith 🔥"
    );
  });

  it('Message should be truncated if total formatted length is over a certain number of characters', () => {
    assert.strictEqual(
      truncateMessage("a".repeat(10), 5, 10, ".."), "aaa.."
    );
  });


});
